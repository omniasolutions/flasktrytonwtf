# -*- coding: utf-8 -*-
##############################################################################
#
#    OmniaSolutions, ERP-PLM-CAD Open Source Solution
#    Copyright (C) 2011-2020 https://OmniaSolutions.website
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this prograIf not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
'''
Created on 10 Aug 2020

@author: mboscolo
'''
import os
import json
import logging
import pickle
import cryptography
from proteus import Model, Wizard, Report
from flask import url_for
from FlaskTrytonWTF.flask_wtf_proteus import TFlaskForm

class Tree(object):
    '''
        this class implements the tree form widget based on a tryton model
    '''
    def __init__(self, proteusConfig,
                 model,
                 xml_rmp_connection_name,
                 user, 
                 srcFilter=[]):
        '''
        Constructor
        :proteusConfig Tryton proteus config
        :model Tryton model
        '''
        self.classprefix = 'flask_wtf_tree_form'
        self.proteusConfig = proteusConfig
        self.model = model
        self.xml_rmp_connection_name = xml_rmp_connection_name
        self.user = user
        self.srcFilter = srcFilter
        self._loadData()

    def _loadData(self):
        #
        # Compute base model data
        #   
        flask_data={}     
        for res in Model.get(self.model, self.proteusConfig).flask_table_data(self.srcFilter,{}):
            flask_data = json.loads(res)
            break
        self.flask_table_header = flask_data.get('HEADER', {})
        self.flask_table_content = flask_data.get('DATA')
        self.table_facilities = flask_data.get('TABLE_FACILITIES', {})
        self.crearable = self.table_facilities.get('creatable', False)
        self.editable = self.table_facilities.get("editable", False)
        self.create = self.table_facilities.get("create", False)
        for field_val in self.flask_table_header.get('HEADER',{}).values():
            if field_val.get("editable"):
                self.editable = True
                break
        self.deletable = False
        self.pagination = False
        
        self.available_reports = self.table_facilities.get('reports',[])
    
    def getIds(self):
        out = []
        for row in self.flask_table_content:
            out.append(row.get('id'))
        return out
        
    def form_edit(self):
        template_file_path = os.path.join(os.path.dirname(__file__), 'templates/form.html') 
        with open(template_file_path, 'r') as f:
            return f.read()

    def render_row(self,
                   headersList_id,
                   ordered_header_index,
                   headers,
                   data):
        record = dict(zip(headersList_id, data.values()))
        record_id = data.get('id')
        out = """<tr  flask_id="%s">""" % self.user.ecriptIds([record_id])
        if self.editable:
            out += """<td><button class="%s_bnt_edit" onclick="edit_record(this, '%s', '%s', '%s' )"><i class="fa fa-edit"></i></button></td>""" % (self.classprefix,
                                                                                                                        self.user.ecriptIds([record_id,]),
                                                                                                                        self.model,
                                                                                                                        self.xml_rmp_connection_name,
                                                                                                                        )
        for index, col in enumerate(ordered_header_index):
            val = data[col]
            if 'tree' not in headers[col].get('visible', ''):
                continue 
            if headers[col].get('type') in ['one2many','many2many']:
                fa_icon = headers[col].get('fa-icon','fa-external-link-alt')
                out += """<td><button class="%s_bnt_edit" onclick="view_releted(this, '%s', '%s', '%s' ,'%s', '%s', '%s')"><i class="fa %s"></i></button></td>""" % (
                                                                                                                                  self.classprefix,               
                                                                                                                                  self.user.ecriptIds(val),
                                                                                                                                  headers[col].get('model_name'),
                                                                                                                                  self.xml_rmp_connection_name,
                                                                                                                                  headers[col].get('string'),
                                                                                                                                  self.model,
                                                                                                                                  self.user.ecriptDict({col: record_id}),
                                                                                                                                  fa_icon)
            else:
                out += """<td>%s</td>""" % val
        if self.deletable:
            out += """<td><button class="%s_td_delete" onclick="delete_record(this, '%s')>X</button></td>""" % (self.classprefix, self.user.ecriptIds([record_id]))
        for report_name in self.available_reports:
            out += """<td><button class="%s_td_delete" onclick="print_record(this, '%s',  '%s', '%s')">R</button></td>""" % (self.classprefix,
                                                                                                                          self.user.ecriptIds([record_id]),
                                                                                                                          self.model,
                                                                                                                          report_name)
        out += """</tr>"""
        return out

    def render_table(self):
        headersLables  = []
        flask_table_header = self.flask_table_header
        ordered_header_index = flask_table_header['ORDERED_HEADER']
        headers = flask_table_header['HEADER']
        for fieldName in ordered_header_index:
            if 'tree' not in headers[fieldName].get('visible', ''):
                continue 
            headersLables.append(headers[fieldName].get('string'))
        out=''
        out += """<table class="w3-table-all w3-hoverable"><thead><tr class="w3-light-grey">""" #% (self.classprefix)
        if self.editable:
            out += """<th class="%s_list">Modifica</th>""" % (self.classprefix)
        if self.pagination:
            #TODO: implement pagination
            pass
        for col in headersLables:
            out += """<th class="%s_list">%s</th>""" % (self.classprefix, col)
        if self.deletable:
            out += """<th class="%s_th_deletable">%s</th>""" % (self.classprefix, 'X')
        for report_name in self.available_reports:
            out += """<th class="%s_th_deletable">%s</th>""" % (self.classprefix, report_name)
        out += """</tr></thead><tbody>"""
        if self.flask_table_content:
            headersList_id = ordered_header_index + ['id']
            #
            for data in self.flask_table_content:
                out+=self.render_row(headersList_id,
                                     ordered_header_index,
                                     headers,
                                     data)
        if self.create:
            out += """<tr><td colspan="100"><button class="fwtf_tree_add_button" onclick="add_record(this,'%s','%s')"><i class="fa fa-plus-square"> Aggiungi</i></button></td></tr>""" %(self.model, self.xml_rmp_connection_name)
        out += """</tbody></table>"""
        return out
         
    def render(self):
        """
            Render generate the table view
        """
        table_name = self.flask_table_header.get('DESCRIPTION','No Table Name')
        out =  """<div class="%s_main_div">""" % self.classprefix
        out += """<link rel="stylesheet" href="/ftwtf/static/css/tree_form_logic.css">"""
        out += """%s""" % (self.form_edit())
        out += """<script src="/ftwtf/static/js/tree_form_logic.js"></script>"""
        out += """<h1 class="%s_h1">%s</h1>""" % (self.classprefix, table_name)
        out += """<ul class="breadcrumb"><li class="breadcrumb"><button class="breadcrumb" onclick="view_releted(this, '%s', '%s', '%s' ,'%s', '%s')">%s</Button></li></ul>""" % (self.user.ecriptIds(self.getIds()),
                                                                                                                                     self.model,
                                                                                                                                     self.xml_rmp_connection_name,
                                                                                                                                     table_name,
                                                                                                                                     self.model,                                                                                                                                  
                                                                                                                                     table_name)
        out += """<div class="table_container_%s">"""% (self.model)
        out += self.render_table()
        out+="""</div></div>"""
        return out

    def form_edit(self):
        template_file_path = os.path.join(os.path.dirname(__file__), 'templates/form.html') 
        with open(template_file_path, 'r') as f:
            return f.read()

class Form(object):
    '''
        this class implements the tree form widget based on a tryton model
    '''
    def __init__(self, proteusConfig, model, id=0):
        self.proteusConfig = proteusConfig
        self.model = model
        self.id = int(id)

    def getEmptyFormClass(self):
        new_form = TFlaskForm
        new_form.trytonObject = Model.get(self.model, self.proteusConfig)
        data = {}
        for data_new in new_form.trytonObject.flask_table_data({}):
            data = json.loads(data_new)
        new_form.tryton_fields = {}
        for fieldName, fieldAttributes in data.get('HEADER',{}).get('HEADER',{}).items():
            if 'form' in fieldAttributes.get("visible"):
                new_form.tryton_fields[fieldName] = fieldAttributes
        return new_form
        
    def getFormClass(self, attributes_value={}):
        new_form = self.getEmptyFormClass()
        new_form._trytonId = self.id
        if attributes_value:
            new_form.setValueFromDict(attributes_value)
        return new_form

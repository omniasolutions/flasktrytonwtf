# -*- coding: utf-8 -*-
##############################################################################
#
#    OmniaSolutions, ERP-PLM-CAD Open Source Solution
#    Copyright (C) 2011-2020 https://OmniaSolutions.website
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this prograIf not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
'''
Created on 10 Aug 2020

@author: mboscolo
'''
import json
import logging
from trytond.pool import Pool

class FlaskTryton(object):
    '''
        this class extend the behaviour of tryton class in order to carry on the flask integration
    '''
    @classmethod
    def flask_header_data(cls, needed_fields, *args, **kargs):
        """
            this method provide data in order to provide tree view creation
        """
        pool = Pool()
        empty_object = pool.get(cls.__name__)()
        out = {}
        out['HEADER'] = {}
        rows = []
        ordered_header = []
        for attributesName, attributeValue in needed_fields.items():
            header_value = None
            fieldsDefinition = empty_object._fields.get(attributesName)
            ordered_header.append(attributesName)
            attribute_vals = {'name': attributesName,
                              'string': fieldsDefinition.string,
                              'help': fieldsDefinition.help,
                              'type': fieldsDefinition._type,
                              'model_name': False,
                              'visible': attributeValue.get('visible', ''),
                              'editable': attributeValue.get('editable'),
                              'fa-icon': attributeValue.get(attributesName, 'fa-external-link-alt')}
            if fieldsDefinition._type in ['one2many','many2many']:
                attribute_vals['model_name'] = fieldsDefinition.model_name
            out['HEADER'][attributesName] = attribute_vals
        out['ORDERED_HEADER'] = ordered_header
        out['DESCRIPTION'] = empty_object.flask_description()
        return out
        
    def flask_description(self):
        """
        define the flask table name
        could be overloaded
        """
        return self.__name__
    
    @classmethod
    def flask_table_data(cls,
                         srcFilter=[('id','=', -1)],
                         stopLevel=True,
                         rec_name=False,
                         crunch=False,
                         *args,
                         **kargs):
        """
        get all the data for flask tree interface
        """
        empty_object = Pool().get(cls.__name__)()
        table_definition = empty_object.define_flask_exposed_field()
        needed_fields = table_definition.get('FIELDS', {})
        header = empty_object.flask_header_data(needed_fields)
        #
        if 'id' not in needed_fields:
            needed_fields["id"]={'name': 'id',
                                 'string': 'ID',
                                 'visible': '',
                                 'editable': False}
        #
        rows = []
        if crunch:
            #todo make here the crunch search
            pass
        for record in empty_object.search(srcFilter) :
            row = {}
            for attributesName in needed_fields:
                header_value = None
                fieldsDefinition = cls._fields.get(attributesName)
                field_type = fieldsDefinition._type
                record_value = record.__getattr__(attributesName)
                if field_type in ['one2many', 'many2many']:
                    if stopLevel:
                        if rec_name:
                            real_value = record_value.get_rec_name('')
                        else:
                            real_value = list(map(lambda x : x.id, record_value))
                    else:
                        real_value = [x.flask_dict_data() for x in record_value]
                elif field_type in ['many2one']:
                    if stopLevel:
                        real_value = record_value.get_rec_name('')
                    else:
                        real_value = record_value.flask_dict_data() 
                elif field_type == 'datetime':
                    real_value = record_value.strftime("%d/%m/%Y %H:%M")
                elif field_type == 'date':
                    real_value = str(record_value)
                elif field_type == 'time':
                    real_value = record_value.strftime("%H:%M")
                else:
                    real_value = record_value
                row[attributesName] = real_value
            row['flask_deletable'] = record.is_flask_deletable()
            rows.append(row)
        #
        return json.dumps({'HEADER': header,
                          'DATA': rows,
                          'TABLE_FACILITIES': table_definition.get('TABLE_FACILITIES', {})}),
    
    def is_flask_deletable(cls, *args, **kargs):
        """
            rule definition if a  particular field can be deleted 
            just show the interface the cancel button
        """
        return False
    
    def define_flask_exposed_field(self):
        """
        this method must be implemented in order to define the data that to expose in tryton
        this function must return a list of name that define the filed exposed
        es: return {'FIELDS': {<field_name>:  {'editable': True/False,
                                               'visible': '', # here option are [tree,form]
                                                'fa-icon': <fa-icon-name>,},},
                    'TABLE_FACILITIES': {'reports': <report_name_0>, <report_name_1>},
                                         'creatable',True/False}
        }
        """
        return {}
    

    @classmethod
    def _setupFlask(cls, RPC):
        """
        open the xml-rpc door for flask
        """
        cls.__rpc__.update({'flask_table_data': RPC(instantiate=None,
                                                    readonly=False)})   
    
    
    
    
# -*- coding: utf-8 -*-
##############################################################################
#
#    OmniaSolutions, ERP-PLM-CAD Open Source Solution
#    Copyright (C) 2011-2020 https://OmniaSolutions.website
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this prograIf not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
'''
Created on 30 Mar 2020

@author: mboscolo
'''
import logging
import datetime
#
from flask_admin.form import DatePickerWidget, DateTimePickerWidget
from flask_wtf import FlaskForm
from wtforms_components import read_only
import wtforms as fields
import wtforms.validators as validator
from .convertion import WTF_TRYTON_FIELD_MAPPING
from .convertion import WTF_TRYTON_FIELD_ATTRIBUTES_MAPPING
from .convertion import WTF_TRYTON_FIELD_ATTRIBUTES_SPECIFIC

from proteus import config, Model, Wizard, Report
  

class TFlaskForm(FlaskForm):
    trytonObject = None
    tryton_fields = {}
    """
    tryton_fields must be a dictionary like object field 
    """
    submitLable = "Submit"
    
    def __init__(self, *arg, **kargs):
        self._read_only_field = []
        self._trytonId = kargs.get('tryton_id', None)
        self.proteusConfig=kargs.get('proteusConfig', None)
        self._unbound_fields = []
        self.GetWtfFromTryton()
        self._unbound_fields.append(('submit', fields.SubmitField(self.submitLable)))
        super(TFlaskForm, self).__init__(*arg, **kargs)
        for field_name in self._read_only_field:
            read_only(self._fields[field_name])
        

    def getMany2OneVals(self, model, fieldName):
        if self.proteusConfig:
            ref_model = Model.get(model, self.proteusConfig)
        else:
            ref_model = self.tryton.pool.get(model)
        fields_to_read = ['id']
        available_fields = list(ref_model._fields)
        if 'nome' in available_fields:
            fields_to_read.append('nome')
        if 'name' in available_fields:
            fields_to_read.append('name')
        if 'id' not in fields_to_read:
            fields_to_read.append('id')
        out = [('', '')]
        curr_model = self.trytonObject._fields
        if self.proteusConfig:
            domain = curr_model[fieldName].get('domain',[])
            to_loop = ref_model.find(eval(domain))
            for ref_obj in to_loop:
                out_str = ''
                for fieldName in fields_to_read:
                    if fieldName != 'id':
                        out_str += '%s - ' % (eval("ref_obj.%s" % fieldName))
                out_str = out_str[:-3]
                out.append((str(ref_obj.id), out_str))
        else:
            domain = curr_model[fieldName].domain
            to_loop = ref_model.search_read(domain, fields_names=fields_to_read)
            for ref_obj in to_loop:
                out_str = ''
                for fieldName in fields_to_read:
                    if fieldName != 'id':
                        out_str += '%s - ' % (ref_obj[fieldName])
                out_str = out_str[:-3]
                out.append((str(ref_obj['id']), out_str))
        return out

    def setupFieldAttributes(self, attribute_to_compute, wtFielAttibutes, field, tryton_field_name, values):
        selection = []
        if isinstance(field, dict):
            fieldType = field.get('type')  
            required = field.get('required')
            selection = field.get('selection')
        else:
            fieldType = field._type
            required = field.required
            if fieldType=='selection':
                selection = field.selection
        #
        for trytonFieldAttribure, WTFFieldAttribute in attribute_to_compute.items():
            if trytonFieldAttribure == 'select':
                wtFielAttibutes[WTFFieldAttribute] = selection
            elif trytonFieldAttribure == 'select_m2o':
                if self.proteusConfig:
                    wtFielAttibutes[WTFFieldAttribute] =  self.getMany2OneVals(field.get('relation'),
                                                                               field.get('name'))
                else:
                    wtFielAttibutes[WTFFieldAttribute] =  self.getMany2OneVals(field.model_name,
                                                                               field.name)
            else:
                if isinstance(field, dict):
                    wtFielAttibutes[WTFFieldAttribute] = field.get(trytonFieldAttribure)
                else:
                    wtFielAttibutes[WTFFieldAttribute] = getattr(field, trytonFieldAttribure)
        if 'label' not in wtFielAttibutes:
            wtFielAttibutes['label'] = tryton_field_name
        wtFielAttibutes['default'] = values.get(tryton_field_name)

            
#         if field._type == 'datetime':
#             #wtFielAttibutes['widget'] = DateTimePickerWidget()
#             wtFielAttibutes['render_kw'] = {'format': '%m/%d/%y'}
        if fieldType == 'date':
            wtFielAttibutes['widget'] = DatePickerWidget()
        if required:
            validators = wtFielAttibutes.get('validators',[])
            if fieldType == 'datetime':
                # validators.append(validator.Required())
                # validators.append(validator.InputRequired())
                # wtFielAttibutes['default'] = datetime.datetime.today
                validators.append(validator.DataRequired())
                wtFielAttibutes['format'] = '%d/%m/%Y %H:%M:%S'
                #wtFielAttibutes['render_kw'] = {'type': 'datetime-local', 'step': 1}
            else:
                validators.append(validator.DataRequired())
            wtFielAttibutes['validators']=validators
        return wtFielAttibutes

    def GetWtfFromTryton(self):
        """
        Get a brand new FlaskForm created from tryton module
        """
        allTrytonFields = list(self.trytonObject._fields.keys())
        default_values = self.trytonObject.default_get(self.tryton_fields, {})
        if not self.tryton_fields:
            tryton_fields = allTrytonFields
        else:
            tryton_fields = list(self.tryton_fields.keys())
        if self._trytonId:
            default_values.update(self.trytonObject.read([self._trytonId], tryton_fields,{})[0])
        for tryton_field_name in tryton_fields :
            
            if tryton_fields and tryton_field_name not in allTrytonFields:
                continue  
            tryton_field = self.trytonObject._fields.get(tryton_field_name)
            field_read_only = not self.tryton_fields.get(tryton_field_name,{}).get('editable', False)
            if isinstance(tryton_field, dict):
                tryton_field_type = tryton_field.get('type')
            else:
                tryton_field_type = tryton_field._type
            wtField = WTF_TRYTON_FIELD_MAPPING.get(tryton_field_type)
            if wtField:
                wtFielAttibutes = {}
                attribute_to_compute = {}
                attribute_to_compute.update(WTF_TRYTON_FIELD_ATTRIBUTES_MAPPING)
                attribute_to_compute.update(WTF_TRYTON_FIELD_ATTRIBUTES_SPECIFIC.get(tryton_field_type, []))
                #
                wtFielAttibutes = self.setupFieldAttributes(attribute_to_compute,
                                                            wtFielAttibutes,
                                                            tryton_field,
                                                            tryton_field_name, 
                                                            default_values)
                #
                new_field = wtField(**wtFielAttibutes)
                self._unbound_fields.append((tryton_field_name, new_field))
                if field_read_only:
                    self._read_only_field.append(tryton_field_name)
            else:
                pass
                #raise NotImplementedError("Widget for filed %r not implemented" % tryton_field_name)
    def setValueFromDict(self, div_value={}):
        for k,v in div_value:
            if k in self._unbound_fields:
                self._unbound_fields[k]['default']=v
        
    def trytonSubmit(self):
        create_vals = {}
        fieldNames = self.tryton_fields.keys()
        for field_name in fieldNames:
            if field_name in self:
                create_vals[field_name] = self.__getitem__(field_name).data
        if not self._trytonId:
            self._trytonId = self.trytonObject.create([create_vals])
        else:
            self.trytonObject.write([int(self._trytonId)], create_vals, {})
        if isinstance(self._trytonId, (list,tuple)):
            ret = self.trytonObject.read(self._trytonId, list(fieldNames))
        else: 
            ret = self.trytonObject.read([int(self._trytonId)], list(fieldNames), {})
        for r in ret :
            return r
        return {}
        
        
             
#!/usr/bin/env python
# coding=utf8
##############################################################################
#
#    OmniaSolutions, ERP-PLM-CAD Open Source Solutions
#    Copyright (C) 2011-2020 https://OmniaSolutions.website
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this prograIf not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
'''
Created on 13 Aug 2020

@author: mboscolo
'''
#
import json
import pickle
from cryptography.fernet import Fernet
#
from flask import request
from flask import redirect
from flask import make_response
from flask import Blueprint
from flask import render_template
#
from flask_login import login_required
#
from FlaskTrytonWTF.TreeForm import Tree, Form
from proteus import config as proteus_config
from proteus import Report
from proteus import Model

flask_tryton_wtf_route = Blueprint('flask_tryton_wtf_route',
                                   __name__,
                                   template_folder='templates',
                                   static_folder='static',
                                   url_prefix='/ftwtf',
                                   )



class DummyUser():
    def __init__(self):
        connection = "http://admin:admin@localhost:8001/mlnv/"
        self.proteusXmlRpcConnections = {'test': proteus_config.set_xmlrpc(connection)} 
        self.cipher_suite = Fernet(Fernet.generate_key())

    def ecriptIds(self, ids=[]):
        return self.cipher_suite.encrypt(pickle.dumps(ids)).decode('utf-8')
    
    def decriptIds(self, encripted):
        ids=[]
        idss = pickle.loads(self.cipher_suite.decrypt(encripted.encode('utf-8')))
        if not isinstance(idss, list):
            if "," in idss:
                for str_id in idss.split(','):
                    try:
                        ids.append(int(str_id))
                    except:
                        pass
        elif  isinstance(idss, list):
            ids=idss
        return ids 

    def ecriptDict(self, val={}):
        return self.cipher_suite.encrypt(pickle.dumps(val)).decode('utf-8')
    
    def decriptDict(self, encripted):
        return pickle.loads(self.cipher_suite.decrypt(encripted.encode('utf-8')))

    
#this must be commented in production 
current_user = DummyUser()

#@login_required
@flask_tryton_wtf_route.route('/tree')
def tree(): 
    current_user.last_tree_section = request.args
    ids_encripted = request.args.get('flask_id')
    ids = current_user.decriptIds(ids_encripted)
    model = request.args.get('flask_model')
    from_model = request.args.get('from_model')
    from_model_id = current_user.decriptDict(request.args.get('from_model_id'))
    service_name = request.args.get('service_name')
    proteusConfig = current_user.proteusXmlRpcConnections.get(service_name)
    if proteusConfig and model != 'None':
        tf = Tree(proteusConfig = proteusConfig,
                  model=model,
                  user=current_user,
                  xml_rmp_connection_name=service_name)
        tf.filter = [('id','in', ids)]
        return render_template("tree.html",
                               available_service=tf,
                               descrizione_servizio=model,
                               from_model=from_model,
                               from_model_id=from_model_id)


    
#@login_required
@flask_tryton_wtf_route.route('/edit_form', methods=['GET', 'POST'])
def edit_form():
    id =0
    default_value_dict = {}
    if request.args.get('flask_id')=='0': # this mean new record
        try:
            default_value_dict=current_user.decriptDict(current_user.last_tree_section.get('from_model_id'))
        except:
            pass
    else:
        for id in current_user.decriptIds(request.args.get('flask_id')):
            break
    model = request.args.get('flask_model')
    service_name = request.args.get('service_name')
    from_model = request.args.get('from_model')
    proteusConfig = current_user.proteusXmlRpcConnections.get(service_name)
    tf = Form(proteusConfig = proteusConfig,
              model=model,
              id=id)
    formClass = tf.getFormClass()
    if default_value_dict:
        formClass.setValueFromDict(default_value_dict)
    form = formClass(tryton_id=id,
                     proteusConfig = proteusConfig)
    if form.validate_on_submit():
        form.trytonSubmit()
        return json.dumps(current_user.last_tree_section)
        #return url_for('servizi_sotto', sottoservizio = service_name)
    return render_template('generic_form_content.html', form=form)                       
 
#@login_required
@flask_tryton_wtf_route.route('/print', methods=['GET'])
def print_record():
    ids_encripted = request.args.get('flask_id')
    ids = current_user.decriptIds(ids_encripted)
    flask_model = request.args.get('flask_model')
    report_name = request.args.get('report_name')
    service_name = request.args.get('service_name')
    proteusConfig = current_user.proteusXmlRpcConnections.get('test')
    r = Report(name=report_name,
               config=proteusConfig)
    type_, data, print_, name = r.execute(data={'ids':ids})
    def generate():
        for row in data:
            yield ','.join(row) + 'n'
    response = make_response(data)
    response.headers['Content-Type'] = 'application/pdf'
    response.headers['Content-Disposition'] = 'inline; filename=%s.pdf' % 'yourfilename'
    return response #make_response(generate(), mimetype='application/pdf')



# @login_required
@flask_tryton_wtf_route.route('/tree_test')
def test_tree(): 
    #http://localhost:5000/ftwtf/tree_test
    xml_rmp_connection_name = 'test'
    tf = Tree(proteusConfig = current_user.proteusXmlRpcConnections.get(xml_rmp_connection_name),
              xml_rmp_connection_name=xml_rmp_connection_name,
              user=current_user,
              model='registry.all')
    tf.filter = ['id','>',0]
    return render_template("test_form.html", tryton_flask_render = tf)
                                 
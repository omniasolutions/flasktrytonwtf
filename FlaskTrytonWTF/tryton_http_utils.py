# -*- coding: utf-8 -*-
##############################################################################
#
#    OmniaSolutions, ERP-PLM-CAD Open Source Solutions
#    Copyright (C) 2011-2020 https://OmniaSolutions.website
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this prograIf not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
'''
Created on 1 Apr 2020

@author: mboscolo
'''
import json
import requests
import base64


def _flask_dict_data(self,
                     filter=[],
                     needed_fields=[],
                     fa_icons={},
                     stopLevel=True):
    out = {}
    out['HEADER'] = {}
    rows = []
    ordered_header = []
    for record in self.search(filter):
        row = {}
        for attributesName in needed_fields:
            header_value = None
            fieldsDefinition = self._fields.get(attributesName)
            record_value = getattr(record, attributesName)
            field_type = fieldsDefinition._type
            cell_info = {'name': attributesName,
                         'string': str(fieldsDefinition.string),
                         'help': str(fieldsDefinition.help),
                         'type': str(fieldsDefinition._type),
                         'fa-icon': fa_icons.get(attributesName, 'fa-external-link-alt')} 
            if field_type in ['one2many','many2many']:
                cell_info['model_name']= fieldsDefinition.model_name
                if not stopLevel:
                   record_value = [x.flask_dict_data() for x in getattr(record, attributesName)]
            elif field_type in ['many2one']:
                cell_info['model_name'] = fieldsDefinition.model_name
                if not stopLevel:
                    record_value = record_value.flask_dict_data() 
            elif field_type == 'datetime':
                record_value = record_value.strftime("%d/%m/%Y %H:%M")
            elif field_type == 'date':
                record_value = record_value.strftime("%d/%m/%Y")
            elif field_type == 'time':
                record_value = record_value.strftime("%H:%M")
            row[attributesName] = record_value 
            ordered_header.append(attributesName)
            out['HEADER'][attributesName] = cell_info

        row['id']=record.id
        rows.append(row)
    out['DATA'] = rows
    out['ORDERED_HEADER'] = ordered_header
    return out


class HttpClient:
    """
    HTTP Client to make JSON RPC requests to Tryton server.
    User is logged in when an object is created.
    """

    def __init__(self,
                 url,
                 database_name,
                 user, 
                 passwd):
        """
        :url full url with port es: http://localhost:8069
        :database_name name of the database that we are gonna to use
        :user tryton user name
        :passwd tryton user password 
        """
        self._url = '{}/{}/'.format(url, database_name)
        self._user = user
        self._passwd = passwd
        self._login()
        self._id = 0

    def get_id(self):
        """
        Get the record internal id each call perform a new id to be created
        :return: int(ID of the record)
        """
        self._id += 1
        return self._id

    def _login(self):
        """
        Perorm a login to the server
        :return:  0 - user id / 1 - session token
        """
        payload = json.dumps({
            'params': [self._user, {'password': self._passwd}],
            'jsonrpc': "2.0",
            'method': 'common.db.login',
            'id': 1,
        })
        headers = {'content-type': 'application/json'}
        result = requests.post(self._url,
                               data=payload,
                               headers=headers)
        if result.status_code in [500, 401]:
            raise Exception(result.text)
        
        if 'json' in result:
            self._session = result.json()['result']
        else:
            self._session = json.loads(result.text)

        return self._session

    def call(self,
             prefix,
             method, 
             params=[[], {}, {}]):
        """
        Perform a post call 
        :prefix rpc call object [model., system,..]
        :method method to call
        :params parameter to pass to the function
        :return: json object
        """
        method = '{}.{}'.format(prefix, method)
        payload = json.dumps({
            'params': params,
            'method': method,
            'id': self.get_id(),
         })
        auth = "%s:%s:%s" % (self._user, self._session[0], self._session[1])
        authorization = base64.b64encode(auth.encode('utf-8'))
        headers = {
        'Content-Type': 'application/json',
        'Authorization': b'Session ' + authorization
        }
        response = requests.post(self._url,
                                 data=payload,
                                 headers=headers)
        if response.status_code in [500, 401]:
            raise Exception(response.text)
        return response.json()

    def model(self,
              model,
              method,
              args=[],
              kwargs={}):
        """
        Perform a model call to a tryton  object
        :model tryton model to perform a call
        :method method to call
        :args args to pass to the call
        :return: json object
        """
        return self.call('model.%s' % model, method, (args, kwargs, {}))

    def system(self,
               method):
        """
        Perform a system method call with no argument
        :method to call
        :return: json object
        """
        return self.call('system', method, params=[])
/**
 * generic_form
 */


function edit_record(elmnt, flask_id, model, service_name, from_id) {
    var form_url = '/ftwtf/edit_form?flask_id=' + flask_id + '&flask_model=' + model + '&service_name=' +  service_name + '&from_id' + from_id;
	var modal = document.getElementById("form_modal");
	var validate = function(){
		modal.style.display = "none";
	};
	
	var confirm = function(){
		console.log("Confirm");
	     $.ajaxSetup({
	         beforeSend: function(xhr, settings) {
	             if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain) {
	                 xhr.setRequestHeader("X-CSRFToken", "{{ form.csrf_token._value() }}")
	             }
	         }
	     })
        $.ajax({
            type: "POST",
            url: form_url,
            data: $('form').serialize(), // serializes the form's elements.
            success: function (data) {
                var arg_last_tree_url = JSON.parse(data)
                view_releted(this, //just but somthing not really used in the function
                             arg_last_tree_url["flask_id"],
                             arg_last_tree_url["flask_model"],
                             arg_last_tree_url["service_name"],
                             arg_last_tree_url["service_description"],
                             arg_last_tree_url["flask_model"],
                             arg_last_tree_url["from_model"],
                             arg_last_tree_url["from_id"]);
            }
        });
        //    e.preventDefault(); // block the traditional submission of the form.
     validate();
	};
	var span = document.getElementsByClassName("close")[0];
	span.onclick = validate;
	var closeButton = document.getElementsByClassName("cancelbtn")[0];
	closeButton.onclick = validate;
	var okButton = document.getElementsByClassName("okbtn")[0];
	okButton.onclick = confirm;
	var modal_body = document.getElementsByClassName("modal-body");
	console.log("Perform Load");
	$(modal_body).load(form_url, function(){
		var submit = document.getElementById("submit");
		submit.style.display="none";	
		modal.style.display = "block";
	});
};

function delete_record(elmnt, flask_id) {
	console.log(elmnt);
	console.log(flask_id);
};

function deleteNextSibilling(element){
    var nextSibilling = element.nextSibling;
    if(nextSibilling){
        deleteNextSibilling(nextSibilling);
        nextSibilling.remove()
        }
}


function view_releted(elmnt, flask_ids, model, service_name, service_description, from_model, from_model_id){
	var form_url = '/ftwtf/tree?flask_id=' + flask_ids + '&flask_model=' + model + '&service_name=' +  service_name +'&service_description=' + service_description + '&from_model=' + from_model +'&from_model_id=' +  from_model_id;
    const xhttp = new XMLHttpRequest();
    xhttp.onload = function() {
        for (const element of document.getElementsByClassName("table_container_" + from_model)){
            element.innerHTML= this.responseText;
            break;
        }
        for (const element of document.getElementsByClassName("breadcrumb")){
            for (const element_there of element.childNodes){
                if (element_there.innerText == service_description){
                    deleteNextSibilling(element_there);
                    return;
                }
            }
            var li = document.createElement("li");
            li.classList.add("breadcrumb");
            var button = document.createElement("button");
            button.innerText = service_description;
            button.onclick = function() {view_releted(this, flask_ids, model, service_name, service_description, from_model, from_model_id);};
            button.classList.add("breadcrumb");
            li.appendChild(button);
            element.appendChild(li);
            break;
        }
        
    }
    xhttp.open("GET", form_url);
    xhttp.send();
};

function print_record(elmnt, flask_id, flask_model, report_name){
	var form_url = '/ftwtf/print?flask_id=' + flask_id + '&flask_model=' + flask_model + '&report_name=' +  report_name;
	downloadURI(form_url, report_name);
};

function downloadURI(uri, name) {
    let link = document.createElement("a");
    link.download = name;
    link.href = uri;
    link.click();
}

function add_record(elmnt, flask_model, service_name, from_id){
    edit_record(elmnt, '0', flask_model, service_name, from_id);
};
function message_log(){
window.alert("sometext");
}


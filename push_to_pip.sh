#!/bin/bash
PIP_BASE="/media/hd2/home/mboscolo/virtual_envs/tryton/bin/activate"
echo "activate Triton Venv"
source $PIP_BASE
echo "Compile"
python3 setup.py sdist bdist_wheel
echo "Push To PyPi"
twine upload dist/*
echo "done"
rm dist -R
rm build -R
rm FlaskTrytonWTF.egg-info
echo "*********   Done   ************"